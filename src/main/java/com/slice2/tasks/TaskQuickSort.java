package com.slice2.tasks;

public class TaskQuickSort {
    public int[] sortArrayQuick(int[] array){
        if (array == null) {
            return null;
        }
        int arrLen = array.length;
        if (arrLen < 2){
            return array;
        }
        int basic = array[0];
        int[] lessBasic = new int[0];
        int[] moreBasic = new int[0];
        int[] tempArr;
        for (int i = 1; i < arrLen; i++){
            if (array[i] < basic){
                tempArr = lessBasic;
                lessBasic = new int[lessBasic.length + 1];
                for (int j = 0; j < tempArr.length; j++){
                    lessBasic[j] = tempArr[j];
                }
                lessBasic[lessBasic.length - 1] = array[i];
            }
            else{
                tempArr = moreBasic;
                moreBasic = new int[moreBasic.length + 1];
                for (int j = 0; j < tempArr.length; j++){
                    moreBasic[j] = tempArr[j];
                }
                moreBasic[moreBasic.length - 1] = array[i];
            }
        }

        int[] lessSorted = sortArrayQuick(lessBasic);
        int[] moreSorted = sortArrayQuick(moreBasic);

        int[] sortedArray = new int[arrLen];
        if (lessSorted.length != 0){
            for (int i = 0; i < lessSorted.length; i++){
                sortedArray[i] = lessSorted[i];
            }
        }
        sortedArray[lessSorted.length] = basic;
        if (moreSorted.length != 0){
            for (int i = 0; i < moreSorted.length; i++){
                sortedArray[lessSorted.length + i + 1] = moreSorted[i];
            }
        }
        return sortedArray;
    }
}
