CREATE SEQUENCE IF NOT EXISTS customer_id_seq;
CREATE TABLE IF NOT EXISTS customers (id int8 NOT NULL PRIMARY KEY, customer_name VARCHAR(60) NOT NULL);
ALTER SEQUENCE customer_id_seq OWNED BY customers.id;
ALTER TABLE customers ALTER COLUMN id SET DEFAULT nextval('customer_id_seq');

CREATE SEQUENCE IF NOT EXISTS product_id_seq;
CREATE TABLE IF NOT EXISTS products (id int8 NOT NULL PRIMARY KEY, product_name VARCHAR(60) NOT NULL, product_description VARCHAR(300) NOT NULL);
ALTER SEQUENCE product_id_seq OWNED BY products.id;
ALTER TABLE products ALTER COLUMN id SET DEFAULT nextval('product_id_seq');

INSERT INTO customers (customer_name) VALUES ('Vlad'), ('Max'), ('Jack'), ('Adam');
INSERT INTO products (product_name, product_description) VALUES ('Tea', 'to drink'), ('Book', 'to read'), ('Car', 'to drive'),
 ('Bed', 'to sleep');

CREATE TABLE IF NOT EXISTS customer_product (customer_id int8 REFERENCES customers (id), product_id int8 REFERENCES products (id));

INSERT INTO customer_product (customer_id, product_id) VALUES (1, 2), (1, 4), (2, 1), (2, 2), (2, 4), (3, 1), (4, 1), (4, 4);

UPDATE products SET product_description = 'Pen'
FROM customer_product JOIN customers ON customer_product.customer_id = customers.id
WHERE products.product_name = 'Book' AND customers.customer_name = 'Vlad';

SELECT customer_name
FROM customers JOIN customer_product ON customers.id = customer_product.customer_id
JOIN products ON products.id = customer_product.product_id WHERE products.product_name = 'Book';

SELECT products.product_name, customers.customer_name
FROM products JOIN customer_product ON products.id = customer_product.product_id
JOIN customers ON customers.id = customer_product.customer_id;
