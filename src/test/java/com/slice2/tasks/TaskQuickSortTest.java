package com.slice2.tasks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TaskQuickSortTest {
    TaskQuickSort cut = new TaskQuickSort();

    static Arguments[] sortArrayQuickTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{}, new int[]{}),
                Arguments.arguments(new int[]{222}, new int[]{222}),
                Arguments.arguments(new int[]{-13, -33, 0, 0, 34, 56, 1}, new int[]{-33, -13, 0, 0, 1, 34, 56}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortArrayQuickTestArgs")
    public void sortArrayQuickTest(int[] array, int[] expected) {
        int[] actual = cut.sortArrayQuick(array);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void sortArrayQuickNullTest() {
        int[] actual = cut.sortArrayQuick(null);
        Assertions.assertNull(actual);
    }
}
